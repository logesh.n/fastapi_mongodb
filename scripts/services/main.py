from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


@app.get("/blog")
def index(limit=10, published: bool = True, sort: Optional[str] = None):
    if published:
        return {"data": f'{limit} published blogs from the database'}
    else:
        return {"data": f'{limit} blogs from the database'}


@app.get("/blog/unpublished")
def unpublished():
    return {"data": "unpublished blogs"}


@app.get("/blog/{id}")
def show(id: int):
    # fetch blog with id = id
    return {"data": id}


@app.get("/blog/{id}/comments")
def comments(id, limit=10):
    # fetch comments with id = id
    return {"data": {"1", "3"}}


class Blog(BaseModel):
    title: str
    body: str
    published: Optional[bool]


@app.post("/createblog")
def create_blog(request: Blog):
    return {"data": f"blog is created with the title as {request.title}"}
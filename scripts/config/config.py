from pymongo import MongoClient

try:
    # connecting with the mongodb
    client = MongoClient('localhost', 27017)

    # accessing db
    database = client["todo_app"]

    # accessing collection
    collection = database["todo_collection"]

    print("Connected Successfully!")

except Exception as e:
    print("Error is >> ", e)

from fastapi import APIRouter
from scripts.config.config import collection
from scripts.core.handlers.models.todos_model import Todo
from scripts.core.engine.schemas.todos_schemas import todo_serializer, todos_serializer
from bson import ObjectId

todo_api_router = APIRouter()


# retrieve data
@todo_api_router.get("/")
async def get_todos():
    todos = todos_serializer(collection.find())
    return {"status": "Ok", "data": todos}


@todo_api_router.get("/{id}")
async def get_todo(id: str):
    todo = todos_serializer(collection.find({"_id": ObjectId(id)}))
    return {"status": "Ok", "data": todo}


@todo_api_router.post("/")
async def post_todo(todo: Todo):
    _id = collection.insert_one(dict(todo))
    todo = todo = todos_serializer(collection.find({"_id": _id.inserted_id}))
    return {"status": "Ok", "data": todo}


# update operation
@todo_api_router.put("/")
async def update_todo(_id: str, todo: Todo):
    collection.find_one_and_update({"_id": ObjectId(_id)}, {"$set": dict(todo)})
    todo = todos_serializer(collection.find({"_id": ObjectId(_id)}))
    return {"status": "Ok", "data": todo}


# delete operation
@todo_api_router.delete("/")
async def delete_todo(_id: str):
    collection.find_one_and_delete({"_id": ObjectId(_id)})
    return {"status": "Ok", "data": []}
